﻿using System.Threading.Tasks;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.Infrastructure.Smtp.Model;

namespace Meimo.Services.Infrastructure.Smtp
{
    public interface IEmailSender
    {
        Task SendEmailAsync(TracedObject<SmtpSendEmailMessage> tracedSmtpSendEmailMessage);
    }
}