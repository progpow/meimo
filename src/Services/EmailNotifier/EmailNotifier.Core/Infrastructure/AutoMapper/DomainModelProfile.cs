﻿using AutoMapper;
using Meimo.Services.EmailNotifier.Core.Domain.Notification;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;

namespace Meimo.Services.EmailNotifier.Core.Infrastructure.AutoMapper
{
    public class DomainModelProfile: Profile
    {
        public DomainModelProfile()
        {
            CreateMap<EmailTemplate, EmailTemplate>()
                .ForMember(p => p.TemplateFields, opt => opt.Ignore());
            CreateMap(typeof(TracedObject<>),typeof(TracedObject<>));
        }
    }
}