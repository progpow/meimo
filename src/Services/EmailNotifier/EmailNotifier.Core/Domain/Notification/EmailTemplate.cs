﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meimo.Services.EmailNotifier.Core.Domain.Notification
{
    public class EmailTemplate : BaseEntity
    {
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }
        [MaxLength(4000)]
        [Required]
        public string TemplateText { get; set; }
        public ICollection<EmailTemplateField> TemplateFields { get; set; }
    }
}