﻿using System;

namespace Meimo.Services.EmailNotifier.Api.Models.Api
{
    /// <summary>
    /// Сокращённая структура ответа шаблона сообщения
    /// </summary>
    public class EmailTemplateShortResponse
    {
        /// <summary>
        /// Идентификатор шаблона сообщения
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Название шаблона сообщения
        /// </summary>
        public string Name { get; set; }
    }
}