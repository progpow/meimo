﻿namespace Meimo.Services.EmailNotifier.Api.Models.Api
{
    /// <summary>
    /// Структура ответа c ключом поля-метки шаблона
    /// </summary>
    public class EmailTemplateFieldResponse
    {
        /// <summary>
        /// Ключ поля-метки для шаблона
        /// </summary>
        public string Key { get; set; }
    }
}