﻿using System;
using System.Collections.Generic;

namespace Meimo.Services.EmailNotifier.Api.Models.Service
{
    public class SendingEmailTemplate
    {
        public string From { get; set; }
        public string Cc { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public Guid EmailTemplateId { get; set; }
        public List<SendingEmailTemplateParameter> EmailTemplateParameters { get; set; }
    }
}