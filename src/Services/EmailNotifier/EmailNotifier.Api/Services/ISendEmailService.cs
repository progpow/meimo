﻿using System.Threading.Tasks;
using Meimo.Services.EmailNotifier.Api.Models.Service;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;

namespace Meimo.Services.EmailNotifier.Api.Services
{
    public interface ISendEmailService
    {
        Task SendEmailAsync(TracedObject<SendingEmail> sendingEmail);
        Task SendEmailTemplateAsync(TracedObject<SendingEmailTemplate> sendingEmailTemplate);
    }
}