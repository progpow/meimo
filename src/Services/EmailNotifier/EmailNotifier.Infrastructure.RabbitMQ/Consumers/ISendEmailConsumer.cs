﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Meimo.Services.EmailNotifier.Infrastructure.Consumers
{
    public interface ISendEmailConsumer:IDisposable
    {
        Task ExecuteAsync(CancellationToken stoppingToken);
        Task StopExecuteAsync(CancellationToken cancellationToken);
    }
}