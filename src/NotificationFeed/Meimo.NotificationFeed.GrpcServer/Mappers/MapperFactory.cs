﻿
using Meimo.NotificationFeed.Core;

namespace Meimo.NotificationFeed.GrpcServer
{
    public static class MapperFactory
    {
        public static CreateNotificationReply ToCreateNotificationReply(
            this NotificationCreatingResultDto source)
        {
            if (source is null) return null;

            return new CreateNotificationReply
            {
                Id = source.Id,
                CreateRetries = source.CreateRetries
            };
        }

        public static NotificationReply ToNotificationReply(this Notification source)
        {
            if (source is null) return null;

            return new NotificationReply
            {
                Id = source.Id,
                PersonId = source.PersonId.ToString(),
                EventId = source.EventId.ToString(),
                CreateRetries = source.CreateRetries,
                HasBeenRead = source.HasBeenRead,
                PushSent = source.PushSent,
                EmailSent = source.EmailSent,
                Message = source.Message
            };
        }

        public static PaginatedNotificationsReply ToPaginatedNotificationsReply(
            this PaginatedNotificationsDto source)
        {
            if (source is null) return null;

            var result = new PaginatedNotificationsReply
            {
                NextPageToken = source.NextPageToken
            };

            source.Notifications.ForEach(x => result.Notifications.Add(x.ToNotificationReply()));

            return result;
        }
    }
}
