﻿namespace Meimo.Person.Account.EmailNotifierClient
{
    /// <summary>
    /// Структура запроса поля-метки шаблона с заполненным значением
    /// </summary>
    public class SendingEmailTemplateParameterRequest
    {
        /// <summary>
        /// Ключ поля-метки шаблона
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Шаблон поля-метки шаблона
        /// </summary>
        public string Value { get; set; }
    }
}
