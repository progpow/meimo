﻿using System.Threading;
using System.Threading.Tasks;

namespace Meimo.Person.Account.EmailNotifierClient
{
    public interface IEmailSender
    {
        Task<bool> SendEmailAsync(string toAddress, string subject, string body);
    }
}
