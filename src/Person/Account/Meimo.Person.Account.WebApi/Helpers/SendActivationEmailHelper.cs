﻿using System;
using System.Threading.Tasks;

using Meimo.Person.Account.EmailNotifierClient;

using Microsoft.Extensions.Logging;

namespace Meimo.Person.Account.WebApi.Helpers
{
    public class SendActivationEmailHelper
    {
        public async static Task<bool> SendActivationRequestAsync(
            IEmailSender emailSender,
            ILogger logger,
            EmailActivationOptions emailActivationOptions,
            string recipientAddress,
            string message,
            string subject = "Активация аккаунта")
        {
            if (string.IsNullOrWhiteSpace(recipientAddress) || string.IsNullOrWhiteSpace(message))
            {
                return false;
            }

            string combinedMessage = !string.IsNullOrWhiteSpace(emailActivationOptions.ActivationUrl)
                ? emailActivationOptions.ActivationUrl + message
                : message;

            try
            {
                return await emailSender.SendEmailAsync(recipientAddress, subject, combinedMessage);
            }
            catch (Exception ex)
            {
                logger.LogWarning(ex, "An error occured during send activation token");
            }

            return false;
        }
    }
}
