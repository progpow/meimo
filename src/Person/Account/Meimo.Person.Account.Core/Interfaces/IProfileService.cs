﻿using System;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Account.Core.Interfaces
{
    public interface IProfileService
    {
        /// <summary>
        /// Gets paginated profile info
        /// </summary>
        /// <param name="pageToken"></param>
        /// <param name="pageSize"></param>
        /// <param name="firstNameStartWith"></param>
        /// <returns></returns>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">
        /// Thrown when person profile with specified page token key not found</exception>
        Task<PaginatedProfilesDto> GetPaginatedProfilesAsync(
            Guid pageToken, int pageSize, string firstNameStartWith);

        Task<ProfileStatusInfoDto> GetProfileStatusAsync(Guid id);

        Task<ProfileShortInfoDto> GetProfileShortInfoAsync(Guid id);

        Task<ProfileMiddleInfoDto> GetProfileMiddleInfoAsync(Guid id);

        Task<ProfileDto> GetProfileInfoAsync(Guid id);

        /// <summary>
        /// Updates person profile
        /// </summary>
        /// <param name="profileUpdateDto"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Thrown when parameter is null</exception>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">
        /// Thrown when person profile with specified key not found</exception>
        Task UpdateProfileAsync(ProfileUpdateDto profileUpdateDto);

        Task<PictureInfoDto> GetPictureInfoAsync(Guid id);

        /// <summary>
        /// Updates person picture and avatar
        /// </summary>
        /// <param name="pictureInfoUpdateDto"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Thrown when parameter is null</exception>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">
        /// Thrown when person profile with specified key not found</exception>
        Task UpdatePictureInfoAsync(PictureInfoUpdateDto pictureInfoUpdateDto);

        Task<string> GetCountryNameAsync(int id);

        Task SendToReplicateAsync(ProfileMiddleInfoDto profileMiddleInfoDto);
    }
}
