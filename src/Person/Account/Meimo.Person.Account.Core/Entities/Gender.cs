﻿using System.Runtime.Serialization;

namespace Meimo.Person.Account.Core.Entities
{
    [DataContract]
    public enum Gender : short
    {
        [EnumMember(Value = "Агендер")]
        Agender = 0,

        [EnumMember(Value = "Женский")]
        Female = 1,

        [EnumMember(Value = "Мужской")]
        Male = 2
    }
}
