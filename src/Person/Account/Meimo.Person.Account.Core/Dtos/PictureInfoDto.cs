﻿namespace Meimo.Person.Account.Core.Dtos
{
    public class PictureInfoDto
    {
        public string Avatar { get; set; }

        public string Picture { get; set; }
    }
}
