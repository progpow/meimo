﻿using System;
using System.Collections.Generic;

namespace Meimo.Person.Account.Core.Dtos
{
    public class PaginatedProfilesDto
    {
        public List<ProfileMiddleInfoDto> Profiles { get; set; }

        public Guid NextPageToken { get; set; }
    }
}
