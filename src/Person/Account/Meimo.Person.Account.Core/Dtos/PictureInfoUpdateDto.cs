﻿using System;

namespace Meimo.Person.Account.Core.Dtos
{
    public class PictureInfoUpdateDto
    {
        public Guid Id { get; set; }

        public string Avatar { get; set; }

        public string Picture { get; set; }
    }
}
