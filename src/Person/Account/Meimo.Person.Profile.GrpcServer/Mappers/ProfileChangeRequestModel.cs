﻿using System;

using Meimo.Person.Account.Core.Dtos;
using Meimo.Person.Account.Core.Entities;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfileChangeRequestModel
    {
        public static ProfileUpdateDto ToRepositoryModel(ProfileChangeRequestModel source)
        {
            if (source is null) return null;

            return new ProfileUpdateDto
            {
                Id = Guid.Parse(source.Id),
                FirstName = source.FirstName,
                LastName = source.LastName,
                Gender = (Gender)source.Gender,
                ShowGender = source.ShowGender,
                Birthday = source.Birthday?.ToDateTimeOffset(),
                ShowBirthday = source.ShowBirthday,
                CountryId = source.CountryId,
                City = source.City
            };
        }
    }
}
