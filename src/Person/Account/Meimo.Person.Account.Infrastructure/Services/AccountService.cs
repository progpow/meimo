﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Dtos;
using Meimo.Person.Account.Core.Entities;
using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.Infrastructure.Data;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;

namespace Meimo.Person.Account.Infrastructure.Services
{
    public class AccountService : IAccountService
    {
        private readonly StoreContext _context;
        private readonly ILoggerFactory _loggerFactory;

        public AccountService(StoreContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _loggerFactory = loggerFactory;
        }

        public async Task<PersonAccount> GetAccountByIdAsync(Guid id)
        {
            return await _context.PersonAccounts.FindAsync(id);
        }

        public async Task<bool> HasPhoneNumberAsync(string phoneNumber)
        {
            return await _context.PersonAccounts.AnyAsync(a => a.Phone == phoneNumber);
        }

        public async Task<bool> HasEmailAsync(string email)
        {
            return await _context.PersonAccounts.AnyAsync(a => a.Email == email);
        }

        public async Task<PersonAccount> CreateAccountAsync(PersonAccount personAccount)
        {
            EntityEntry entityEntry = _context.PersonAccounts.Add(personAccount);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger<AccountService>();
                logger.LogWarning(ex, "An error occured during create person account");

                return null;
            }

            await entityEntry.ReloadAsync();

            return await GetAccountByIdAsync(personAccount.Id);
        }

        public async Task<PersonAccount> GetAccountByLoginAndPasswordAsync(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(password))
            {
                return null;
            }

            var personAccount = await _context.PersonAccounts
                .FirstOrDefaultAsync(a => a.Email == login && a.Password == password);

            if (personAccount == null)
            {
                personAccount = await _context.PersonAccounts
                    .FirstOrDefaultAsync(a => a.Phone == login && a.Password == password);
            }

            return personAccount;
        }

        public async Task<LoginDto> GetLoginByIdAsync(Guid id)
        {
            var login = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Select(a => new LoginDto(a.Phone, a.Email))
                .FirstOrDefaultAsync();

            return login;
        }

        public async Task<bool> UpdatePhoneAsync(Guid id, string phone)
        {
            var personAccount = await GetAccountByIdAsync(id);

            if (personAccount == null)
            {
                return false;
            }

            try
            {
                personAccount.Phone = phone;
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                return false;
            }

            var updatedPhone = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Select(a => a.Phone)
                .FirstOrDefaultAsync();

            if (phone != updatedPhone)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> UpdateEmailAsync(Guid id, string email)
        {
            var personAccount = await GetAccountByIdAsync(id);

            if (personAccount == null)
            {
                return false;
            }

            try
            {
                personAccount.Email = email;
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                return false;
            }

            var updatedEmail = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Select(a => a.Email)
                .FirstOrDefaultAsync();

            if (email != updatedEmail)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> UpdatePasswordAsync(
            Guid id,
            string password,
            DateTimeOffset passwordDate)
        {
            var personAccount = await GetAccountByIdAsync(id);

            if (personAccount == null)
            {
                return false;
            }

            personAccount.Password = password;
            personAccount.PasswordDate = passwordDate;

            await _context.SaveChangesAsync();

            var updatedPassword = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Select(a => a.Password)
                .FirstOrDefaultAsync();

            if (password != updatedPassword)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ActivateAccountAsync(Guid id)
        {
            var activationToken = await _context.ActivationTokens.FindAsync(id);

            if (activationToken == null)
            {
                return false;
            }

            var personAccount = await GetAccountByIdAsync(id);

            if (personAccount == null)
            {
                return false;
            }

            _context.ActivationTokens.Remove(activationToken);
            personAccount.Activated = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger<AccountService>();
                logger.LogWarning(ex, "An error occured during account activation");
                return false;
            }

            return true;
        }
    }
}
