﻿
using Meimo.Person.Account.Core.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Meimo.Person.Account.Infrastructure.Data
{
    public class StoreContext : DbContext
    {
        public DbSet<PersonAccount> PersonAccounts { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<ActivationToken> ActivationTokens { get; set; }

        public StoreContext(DbContextOptions<StoreContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema("person");

            modelBuilder.Entity<PersonAccount>(entity =>
            {
                entity.HasIndex(a => a.Phone).IsUnique();
                entity.HasIndex(a => a.Email).IsUnique();

                entity
                    .HasOne(a => a.Country)
                    .WithMany()
                    .HasForeignKey(a => a.CountryId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.Property(a => a.Gender)
                    .HasConversion(new EnumToNumberConverter<Gender, short>());

                entity.Property(a => a.SysLanguage)
                    .HasConversion(new EnumToNumberConverter<SysLanguage, short>());

                entity.Property(a => a.SysRole)
                    .HasConversion(new EnumToNumberConverter<SysRole, int>());
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasIndex(c => c.Name).IsUnique();
                entity.HasIndex(c => c.InvariantName).IsUnique();
            });

            modelBuilder.Entity<ActivationToken>(entity =>
            {
                entity.HasIndex(a => a.Token).IsUnique();
            });
        }
    }
}
