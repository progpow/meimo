﻿using Meimo.Person.AdditionalInfo.Core.Domain;
using Meimo.Person.AdditionalInfo.DataAccess.Data;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meimo.Person.AdditionalInfo.DataAccess.Services
{
    public class AdditionalInfoService
    {

        private readonly IMongoCollection<AdditionalInfoEntity> _additionalInfos;

        public AdditionalInfoService(AdditionalInfoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _additionalInfos = database.GetCollection<AdditionalInfoEntity>(settings.AdditionalInfoCollectionName);
        }

        public async Task<AdditionalInfoEntity> CreatePersonAdditionalInfo(AdditionalInfoEntity additionaInfo)
        {
            await _additionalInfos.InsertOneAsync(additionaInfo);
            return additionaInfo;
        }

        public async Task<AdditionalInfoEntity> GetPersonAdditionalInfo(Guid personId)
        {
            var additionaInfo = await _additionalInfos.FindAsync(x => x.PersonId == personId);
            return additionaInfo.FirstOrDefault();
        }

        public async Task<AdditionalInfoEntity> UpdatePersonAdditionalInfo(AdditionalInfoEntity personData)
        {
            var additionaInfo = await _additionalInfos.FindAsync(x => x.PersonId == personData.PersonId);

            var entity = additionaInfo.FirstOrDefault();
            personData.Id = entity.Id;

            await _additionalInfos.ReplaceOneAsync(x => x.PersonId == personData.PersonId, personData);
            
            var updatedInfo = await _additionalInfos.FindAsync(x => x.PersonId == personData.PersonId);
            return updatedInfo.FirstOrDefault();
        }

        public async Task<bool> DeletePersonAdditionalInfo(Guid personId)
        {
            var additionaInfo = await _additionalInfos.DeleteOneAsync(x => x.PersonId == personId);
            return true;
        }

        public async Task<List<AdditionalInfoEntity>> GetAllPersonAdditionalInfo()
        {
            var additionaInfo = await _additionalInfos.Find(_ => true).ToListAsync();
            return additionaInfo;
        }
    }
}
