﻿using Meimo.Person.AdditionalInfo.DataAccess.Services;
using Meimo.Person.AdditionalInfo.WebApi.Helpers;
using Meimo.Person.AdditionalInfo.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meimo.Person.AdditionalInfo.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AdditionalInfoController : ControllerBase
    {
        private readonly AdditionalInfoService _additionalInfoService;

        public AdditionalInfoController(AdditionalInfoService additionalInfoService)
        {
            _additionalInfoService = additionalInfoService;
        }

        /// <summary>
        /// Создать доп. информацию пользователя
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<PersonAdditionalInfoResponse>> CreatePersonAdditionalInfoAsync(CreatePersonAdditionalInfoRequest request)
        {
            var entity = request.MapRequestToEntity();
            var result = await _additionalInfoService.CreatePersonAdditionalInfo(entity);
            var mapResult = result.MapEntityToResponse();

            return Ok(mapResult);
        }


        /// <summary>
        /// Получить доп. информацию пользователя
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [HttpGet("{personId:guid}")]
        public async Task<ActionResult<PersonAdditionalInfoResponse>> GetPersonAdditionalInfoAsync(Guid personId)
        {
            if (personId == null)
                return BadRequest(personId);

            var result = await _additionalInfoService.GetPersonAdditionalInfo(personId);
            var mapResult = result.MapEntityToResponse();

            return Ok(mapResult);
        }


        /// <summary>
        /// Обновить доп. информацию пользователя
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<PersonAdditionalInfoResponse>> UpdatePersonAdditionalInfoAsync(CreatePersonAdditionalInfoRequest request)
        {
            var entity = request.MapRequestToEntity();
            var result = await _additionalInfoService.UpdatePersonAdditionalInfo(entity);
            var mapResult = result.MapEntityToResponse();

            return Ok(mapResult);
        }


        /// <summary>
        /// Удалить доп. информацию пользователя
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [HttpDelete("{personId:guid}")]
        public async Task<ActionResult> DeletePersonAdditionalInfoAsync(Guid personId)
        {
            if (personId == null)
                return BadRequest(personId);

            var result = await _additionalInfoService.DeletePersonAdditionalInfo(personId);

            if (!result)
                return Problem();
            return Ok();
        }

        /// <summary>
        /// Получить доп. информацию пользователя по всем пользователям
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PersonAdditionalInfoResponse>>> GetAllPersonAdditionalInfoAsync()
        {
            var result = await _additionalInfoService.GetAllPersonAdditionalInfo();
            var mapResult = result.Select(x => x.MapEntityToResponse()).ToList();

            return Ok(mapResult);
        }
    }
}
