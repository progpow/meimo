using System.Text.Json;

namespace Meimo.Friends.Kafka.Contracts
{
    public class FriendsEventMessageSerializer
    {
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public FriendsEventMessageSerializer()
        {
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true,
                WriteIndented = true,
            };
        }

        public string Serialize(FriendsEventMessage friendsEventMessage)
        {
            return JsonSerializer.Serialize(friendsEventMessage, _jsonSerializerOptions);
        }
    }
}