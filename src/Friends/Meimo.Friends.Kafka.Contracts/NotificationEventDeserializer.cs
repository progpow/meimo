using System;
using System.Text.Json;
using Confluent.Kafka;

namespace Meimo.Friends.Kafka.Contracts
{
    public class NotificationEventDeserializer : IDeserializer<NotificationEvent>
    {
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public NotificationEventDeserializer()
        {
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true,
                WriteIndented = true,
            };
        }
   
        public NotificationEvent Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
        {
            if (isNull) return new NotificationEvent { IsNull = true };
            
            var result = JsonSerializer.Deserialize<NotificationEvent>(data, _jsonSerializerOptions);

            if (result is null) return new NotificationEvent { IsNull = true };
            
            if (result.Id == Guid.Empty || 
                result.DestinationId == Guid.Empty ||
                string.IsNullOrWhiteSpace(result.Message)) return new NotificationEvent { IsEmpty = true };
            
            return result;
        }
    }
}