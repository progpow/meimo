using System.Text.Json;
using Confluent.Kafka;

namespace Meimo.Friends.Kafka.Contracts
{
    public class NotificationEventSerializer: ISerializer<NotificationEvent>
    {
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public NotificationEventSerializer()
        {
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true,
                WriteIndented = true,
            };
        }

        public byte[] Serialize(NotificationEvent data, SerializationContext context)
        {
            return JsonSerializer.SerializeToUtf8Bytes(data, _jsonSerializerOptions);
        }
    }
}