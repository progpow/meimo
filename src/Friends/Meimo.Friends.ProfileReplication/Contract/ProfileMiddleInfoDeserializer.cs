using System;
using System.Text.Json;
using Confluent.Kafka;

namespace Meimo.Friends.ProfileReplication.Contract
{
    public class ProfileMiddleInfoDeserializer :IDeserializer<ProfileMiddleInfo>
    {
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public ProfileMiddleInfoDeserializer()
        {
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true,
                WriteIndented = true,
            };
        }

        public ProfileMiddleInfo Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
        {
            if (isNull) return null;

            var jsonResult = Deserializers.Utf8.Deserialize(data, isNull, context);
            var result = JsonSerializer.Deserialize<ProfileMiddleInfo>(jsonResult, _jsonSerializerOptions);

            return result;
        }
    }
}