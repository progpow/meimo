using System.Collections.Generic;

namespace Meimo.Friends.Core
{
    public class SortOrder
    {
        public IReadOnlyList<SortingItem> SortingItems { get; }

        public SortOrder(IReadOnlyList<SortingItem> sortingItems)
        {
            SortingItems = sortingItems;
        }
    }
}
