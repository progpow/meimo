namespace Meimo.Friends.Core
{
    public record SortingItem
    {
        public SortingField SortingField { get; set; }

        public SortingDirection SortingDirection { get; set; }
    }
}
