using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Meimo.Friends.Core
{
    public interface IRelationshipRepository
    {
        /// <summary>
        /// Создать запрос на добавление в друзья
        /// </summary>
        /// <param name="person1">Инициатор запроса</param>
        /// <param name="person2">Адресат запроса</param>
        /// <returns></returns>
        Task<MergeResult> MakeFriendRequest(Guid person1, Guid person2);
        
        /// <summary>
        /// Принять в друзья
        /// </summary>
        /// <param name="person1">Пользователь принимающий в друзья</param>
        /// <param name="person2">Пользователь создавший запрос</param>
        /// <returns></returns>
        Task<GraphModifyResult> AcceptFriendRequest(Guid person1, Guid person2);
        
        /// <summary>
        /// Оставить в подписчиках из состояний запрос в друзья и друзья
        /// </summary>
        /// <param name="person1">Инициатор действия</param>
        /// <param name="person2">Пользователь создавший запрос в друзья или друг</param>
        /// <returns></returns>
        Task<GraphModifyResult> LeaveInSubscribers(Guid person1, Guid person2);
        
        /// <summary>
        /// Подписаться на пользователя
        /// </summary>
        /// <param name="person1">Пользователь который подписывается</param>
        /// <param name="person2">Пользователь на которого подписываются</param>
        /// <returns></returns>
        Task<MergeResult> Subscribe(Guid person1, Guid person2);
        
        /// <summary>
        /// Отменить подписку на пользователя
        /// </summary>
        /// <param name="person1">Подписанный пользователь который отменяет подписку</param>
        /// <param name="person2">Пользователь на которого сделана подписка</param>
        /// <returns></returns>
        Task<GraphModifyResult> Unsubscribe(Guid person1, Guid person2);
        
        /// <summary>
        /// Удалить из подписчиков
        /// </summary>
        /// <param name="person1">Пользователь на которого сделана подписка</param>
        /// <param name="person2">Подписчик</param>
        /// <returns></returns>
        Task<GraphModifyResult> RemoveFromSubscribers(Guid person1, Guid person2);
        
        /// <summary>
        /// Заблокировать - добавить в черный список
        /// </summary>
        /// <param name="person1">Кто блокирует</param>
        /// <param name="person2">Кого блокирует</param>
        /// <returns></returns>
        Task<MergeResult> AddToBlacklist(Guid person1, Guid person2);
        
        /// <summary>
        /// Разблокировать - удалить из черного списка
        /// </summary>
        /// <param name="person1">Кто разблокирует</param>
        /// <param name="person2">Кого разблокирует</param>
        /// <returns></returns>
        Task<GraphModifyResult> RemoveFromBlacklist(Guid person1, Guid person2);

        /// <summary>
        /// Получить все связи между двумя пользователями
        /// (person1)-RelationType-(person2)
        /// </summary>
        /// <param name="person1"></param>
        /// <param name="person2"></param>
        /// <returns></returns>
        Task<IList<Relationship>> GetRelationships(Guid person1, Guid person2);

        /// <summary>
        /// Получить состояние блокировки пользователя person1 пользователем person2
        /// </summary>
        /// <param name="person1"></param>
        /// <param name="person2"></param>
        /// <returns></returns>
        Task<bool> GetIsBlockedBy(Guid person1, Guid person2);
    }
}
