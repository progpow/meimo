using System;
using System.Threading.Tasks;

namespace Meimo.Friends.Core
{
    public interface IPersonRepository
    {
        /// <summary>
        /// Получить статистику по количеству друзей, заявок, подписок и блокировок
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        Task<PersonStatistics?> GetPersonStatistics(Guid personId);

        /// <summary>
        /// Получить связанных пользователей по типу связи
        /// с фильтрацией сортировкой и пагинацией 
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="relationType"></param>
        /// <param name="filter"></param>
        /// <param name="sortOrder"></param>
        /// <param name="paginationFilter"></param>
        /// <returns></returns>
        Task<PagedResponse<Person>> GetRelatedPersonsByRelationType(
            Guid personId,
            RelationType relationType, 
            Filter? filter = null,
            SortOrder? sortOrder = null,
            PaginationFilter? paginationFilter = null);

        /// <summary>
        /// Записать в ноду пользователя информацию профиля
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        Task<bool> SetPersonProfileInfo(Person person);
    }
}
