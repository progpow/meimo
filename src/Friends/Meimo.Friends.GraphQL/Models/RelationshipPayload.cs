using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using Meimo.Friends.Core;

namespace Meimo.Friends.GraphQL.Models
{
    public class RelationshipPayload : Payload
    {
        public Relationship? Relationship { get; }

        public RelationshipPayload(Relationship? relationship)
        {
            Relationship = relationship;
        }
        
        public RelationshipPayload(IReadOnlyList<UserError>? errors) : base(errors) { }
        
        public RelationshipPayload(UserError error) : base(new[] { error }) { }
        
        public async Task<IEnumerable<Relationship>> GetRelationships([Service] IRelationshipRepository repository)
        {
            if (Relationship is null) return new List<Relationship>();
            return await repository.GetRelationships(Relationship.FirstPersonId, Relationship.SecondPersonId);
        }
    }
}
