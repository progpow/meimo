using System;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Meimo.Friends.Core;

namespace Meimo.Friends.GraphQL.TypeExtensions
{
    [ExtendObjectType(Constants.QueryTypeName)]
    public class PersonQueries
    {
        public async Task<PersonStatistics?> GetPersonStatistics(
            Guid personId, [Service] IPersonRepository repository)
        {
            return await repository.GetPersonStatistics(personId);
        }

        public async Task<PagedResponse<Person>> GetRelatedPersonsByRelationType(
            [Service] IPersonRepository repository,
            Guid personId,
            RelationType relationType,
            Filter? filter = null,
            SortOrder? sortOrder = null,
            PaginationFilter? paginationFilter = null)
        {
            return await repository.GetRelatedPersonsByRelationType(
                personId,
                relationType,
                filter,
                sortOrder,
                paginationFilter);
        }
    }
}
