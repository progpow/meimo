using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Meimo.Friends.Core;
using Neo4j.Driver;

namespace Meimo.Friends.Infrastructure
{
    public class PersonRepository : IPersonRepository
    {
        private const string IdParameterName = "id";
        private const string GuidParameterName = "guid";
        
        private const string PNodeSelectorStatement =
            "(p:Person{id: $" + IdParameterName + ", guid: $" + GuidParameterName + "})";

        private const string PersonStatisticsStatementTemplate =
            "MATCH " + PNodeSelectorStatement + "\n" +
            "RETURN\n" +
            "coalesce(p.numberOfFriends, 0) AS " + nameof(PersonStatistics.Friends) + ",\n" +
            "coalesce(p.numberOfIncomingRequests, 0) AS " + nameof(PersonStatistics.IncomingRequests) + ",\n" +
            "coalesce(p.numberOfOutgoingRequests, 0) AS " + nameof(PersonStatistics.OutgoingRequests) + ",\n" +
            "coalesce(p.numberOfIncomingSubscriptions, 0) AS " + nameof(PersonStatistics.IncomingSubscriptions) + ",\n" +
            "coalesce(p.numberOfOutgoingSubscriptions, 0) AS " + nameof(PersonStatistics.OutgoingSubscriptions) + ",\n" +
            "coalesce(p.numberOfIncomingLocks, 0) AS " + nameof(PersonStatistics.IncomingLocks) + ",\n" +
            "coalesce(p.numberOfOutgoingLocks, 0) AS " + nameof(PersonStatistics.OutgoingLocks);

        private readonly IDriver _driver;
        private readonly IFriendsDbSettings _friendsDbSettings;

        public PersonRepository(IDriver driver, IFriendsDbSettings friendsDbSettings)
        {
            _driver = driver;
            _friendsDbSettings = friendsDbSettings;
        }

        private IAsyncSession GetAsyncSession(AccessMode defaultAccessMode)
        {
            return _driver.AsyncSession(builder => builder
                .WithDatabase(_friendsDbSettings.DatabaseName)
                .WithDefaultAccessMode(defaultAccessMode));
        }
        
        private static Dictionary<string, object> CreatePersonStatementParameters(Guid personId)
        {
            return new Dictionary<string, object>
            {
                { IdParameterName, personId.ToString() },
                { GuidParameterName, personId.ToByteArray() }
            };
        }

        public async Task<PersonStatistics?> GetPersonStatistics(Guid personId)
        {
            var statementParameters = CreatePersonStatementParameters(personId);
            var session = GetAsyncSession(AccessMode.Read);
            
            try
            {
                var cursor = await session.RunAsync(PersonStatisticsStatementTemplate, statementParameters);

                if (!await cursor.FetchAsync()) return null;
                
                var personStatistics = new PersonStatistics
                {
                    Id = personId,
                    Friends = cursor.Current[nameof(PersonStatistics.Friends)].As<int>(),
                    IncomingRequests = cursor.Current[nameof(PersonStatistics.IncomingRequests)].As<int>(),
                    OutgoingRequests = cursor.Current[nameof(PersonStatistics.OutgoingRequests)].As<int>(),
                    IncomingSubscriptions = cursor.Current[nameof(PersonStatistics.IncomingSubscriptions)].As<int>(),
                    OutgoingSubscriptions = cursor.Current[nameof(PersonStatistics.OutgoingSubscriptions)].As<int>(),
                    IncomingLocks = cursor.Current[nameof(PersonStatistics.IncomingLocks)].As<int>(),
                    OutgoingLocks = cursor.Current[nameof(PersonStatistics.OutgoingLocks)].As<int>()
                };

                return personStatistics;
            }
            finally
            {
                await session.CloseAsync();
            }
        }
        
        public async Task<PagedResponse<Person>> GetRelatedPersonsByRelationType(
            Guid personId, 
            RelationType relationType, 
            Filter? filter = null,
            SortOrder? sortOrder = null,
            PaginationFilter? paginationFilter = null)
        {
            var relationText = relationType switch
            {
                RelationType.Friend => "-[:FRIEND]-(p2)",
                RelationType.RequestFrom => "<-[:REQUEST]-(p2)",
                RelationType.RequestTo => "-[:REQUEST]->(p2)",
                RelationType.SubscriptionFrom => "<-[:SUBSCRIBED_TO]-(p2)",
                RelationType.SubscriptionTo => "-[:SUBSCRIBED_TO]->(p2)",
                RelationType.BlockedBy => "<-[:HAS_BLOCKED]-(p2)",
                RelationType.HasBlocked => "-[:HAS_BLOCKED]->(p2)",
                _ => string.Empty
            };

            if (string.IsNullOrEmpty(relationText))
                return new PagedResponse<Person>(new List<Person>());

            var statementParameters = CreatePersonStatementParameters(personId);
            
            // Фильтр
            var filterText = string.Empty;

            if (filter is not null)
            {
                var andText = string.Empty;
                
                if (!string.IsNullOrWhiteSpace(filter.FirstName))
                {
                    statementParameters.Add("pFirstName", filter.FirstName.ToLower());
                    filterText = " p2.firstNameLowerCase CONTAINS $pFirstName\n";
                    andText = "  AND";
                }
                
                if (!string.IsNullOrWhiteSpace(filter.LastName))
                {
                    statementParameters.Add("pLastName", filter.LastName.ToLower());
                    filterText += andText + " p2.lastNameLowerCase CONTAINS $pLastName\n";
                    andText = "  AND";
                }

                if (!string.IsNullOrWhiteSpace(filter.City))
                {
                    statementParameters.Add("pCity", filter.City.ToLower());
                    filterText += andText + " p2.cityLowerCase CONTAINS $pCity\n";
                }
            }

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                filterText = "WHERE" + filterText;
            }
            
            // Сортировка
            var sortOrderText = string.Empty;
            
            if (sortOrder?.SortingItems is not null && sortOrder.SortingItems.Count > 0)
            {
                sortOrderText = sortOrder.SortingItems
                    .Where(sortingItem => sortingItem.SortingField !=  SortingField.None)
                    .Aggregate(sortOrderText, (current, sortingItem) =>
                    {
                        var sortingFieldText = sortingItem.SortingField switch
                        {
                            SortingField.FirstName => "p2.firstNameLowerCase",
                            SortingField.LastName => "p2.lastNameLowerCase",
                            SortingField.City => "p2.cityLowerCase",
                            SortingField.Id => "p2.guid",
                            _ => string.Empty
                        };

                        if (sortingItem.SortingDirection == SortingDirection.Desc &&
                            sortingFieldText != string.Empty)
                        {
                            sortingFieldText += " DESC";
                        }
                        
                        if (current != string.Empty && sortingFieldText != string.Empty)
                        {
                            return current + ", " + sortingFieldText;
                        }

                        return current + sortingFieldText;
                    });

                if (!string.IsNullOrEmpty(sortOrderText))
                {
                    sortOrderText = "ORDER BY " + sortOrderText;
                }
            }
            
            // Пагинация
            var paged = paginationFilter is not null;
            var pageNumber = paginationFilter?.PageNumber??default;
            var pageSize = paginationFilter?.PageSize??default;
            int totalRecords = default;
            var paginationFilterText = paged ?"\nSKIP $pSkip LIMIT $pLimit" : string.Empty;
            
            if (paged)
            {
                statementParameters.Add("pSkip", (pageNumber-1) * pageSize);
                statementParameters.Add("pLimit", pageSize);
            }
            
            // Шаблон запроса
            var getRelatedPersonsStatementTemplate =
                "MATCH " + PNodeSelectorStatement + relationText + "\n" +
                filterText +
                "RETURN p2.guid AS byteGuid, p2.firstName AS FirstName, p2.lastName AS LastName, p2.avatar AS Avatar,\n" +
                "       p2.city AS City, p2.country AS Country, p2.locked AS Locked, p2.deleted AS Deleted\n" +
                sortOrderText + 
                paginationFilterText;
            
            // Подсчет тотал
            var totalStatementTemplate =
                "MATCH " + PNodeSelectorStatement + relationText + "\n" + filterText + "RETURN count(*) as total";

            var session = GetAsyncSession(AccessMode.Read);
            try
            {
                // Тотал
                if (paginationFilter is not null)
                {
                    var totalCursor = await session.RunAsync(totalStatementTemplate, statementParameters);
                
                    if (await totalCursor.FetchAsync())
                    {
                        totalRecords = totalCursor.Current["total"].As<int>();
                    }

                    await totalCursor.ConsumeAsync();
                }
                
                // Основной запрос
                var cursor = await session.RunAsync(getRelatedPersonsStatementTemplate, statementParameters);

                var data = await cursor.ToListAsync(r => new Person
                {
                    Id = new Guid(r["byteGuid"].As<byte[]>()),
                    FirstName = r["FirstName"].As<string>(),
                    LastName = r["LastName"].As<string>(),
                    Avatar = r["Avatar"].As<string>(),
                    City = r["City"].As<string>(),
                    Country = r["Country"].As<string>(),
                    Locked = r["Locked"].As(false),
                    Deleted = r["Deleted"].As(false)
                });

                await cursor.ConsumeAsync();
                
                var pagedResponse = new PagedResponse<Person>(
                    data: data,
                    paged: paged,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    totalPages: totalRecords / pageSize + (totalRecords % pageSize > 0 ? 1 : 0),
                    totalRecords: totalRecords);                
                
                return pagedResponse;
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        public async Task<bool> SetPersonProfileInfo(Person person)
        {
            if (person.FirstName is null) throw new ArgumentException(
                $"Не задано обязательное поле {person.FirstName}", nameof(person));
            
            var setPersonProfileInfoStatementTemplate =
                "MATCH " + PNodeSelectorStatement + "\n" +
                "SET p.name = $pName,\n" +
                "    p.firstName = $pFirstName, p.firstNameLowerCase = toLower($pFirstName),\n" +
                "    p.lastName = $pLastName, p.lastNameLowerCase = toLower($pLastName),\n" +
                "    p.avatar = $pAvatar,\n" +
                "    p.city = $pCity, p.cityLowerCase = toLower($pCity),\n" +
                "    p.country = $pCountry, p.locked = $pLocked, p.deleted = $pDeleted";

            var statementParameters = CreatePersonStatementParameters(person.Id);
            statementParameters.Add("pFirstName", person.FirstName);
            statementParameters.Add("pLastName", person.LastName!);
            statementParameters.Add("pAvatar", person.Avatar!);
            statementParameters.Add("pCity", person.City!);
            statementParameters.Add("pCountry", person.Country!);
            statementParameters.Add("pLocked", person.Locked);
            statementParameters.Add("pDeleted", person.Deleted);
            var name = person.FirstName +
                       (!string.IsNullOrEmpty(person.LastName) ? " " + person.LastName : string.Empty);
            statementParameters.Add("pName", name);
            
            var session = GetAsyncSession(AccessMode.Write);
            
            try
            {
                var cursor = await session.RunAsync(setPersonProfileInfoStatementTemplate, statementParameters);

                return (await cursor.ConsumeAsync()).Counters.PropertiesSet > 0;
            }
            finally
            {
                await session.CloseAsync();
            }
        }
    }
}
