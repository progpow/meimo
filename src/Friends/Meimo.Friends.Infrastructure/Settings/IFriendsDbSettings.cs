namespace Meimo.Friends.Infrastructure
{
    public interface IFriendsDbSettings
    {
        string? ConnectionString { get; set; }
        string? User { get; set; }
        string? Password { get; set; }
        string? DatabaseName { get; set; }
    }
}
